# Firebase Resource Gradle plugin

This is a Gradle plugin to generate resource values for Firebase from google-services.json file. It uses Variants API and sets res values during configuration phase.

It is a reimagining of the [google-services](https://github.com/google/play-services-plugins/tree/master/google-services-plugin) plugin from Google that is written in Kotlin and uses the modern tools to produce the config values needed for Firebase.
