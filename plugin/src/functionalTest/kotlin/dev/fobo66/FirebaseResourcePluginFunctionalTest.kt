/*
 * Copyright 2023 Andrey Mukamolov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.fobo66

import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.io.TempDir
import java.io.File
import kotlin.test.Test
import kotlin.test.assertTrue

/**
 * A simple functional test for the 'dev.fobo66.greeting' plugin.
 */
class FirebaseResourcePluginFunctionalTest {

    @Test
    fun `can run task`(@TempDir directory: File) {
        // Setup the test build
        directory.resolve("settings.gradle").writeText("")
        directory.resolve("build.gradle").writeText(
            """
plugins {
    id('dev.fobo66.firebase-resource-plugin')
}
"""
        )

        // Run the build
        val runner = GradleRunner.create().forwardOutput().withPluginClasspath().withArguments("greeting")
            .withProjectDir(directory)
        val result = runner.build()

        // Verify the result
        assertTrue(result.output.contains("Hello from plugin 'dev.fobo66.greeting'"))
    }
}
