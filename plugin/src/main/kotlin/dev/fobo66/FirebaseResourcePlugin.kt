/*
 * Copyright 2024 Andrey Mukamolov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.fobo66

import com.android.build.api.variant.AndroidComponentsExtension
import com.android.build.api.variant.ResValue
import dev.fobo66.entities.GoogleServices
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.findByType

/**
 * Gradle plugin to generate resource values for Firebase from google-services.json file.
 * It uses Variants API and sets res values during configuration phase.
 */
@Suppress("unused")
class FirebaseResourcePlugin : Plugin<Project> {
    @OptIn(ExperimentalSerializationApi::class)
    override fun apply(project: Project) {
        project.extensions.create<FirebaseResourcePluginConfig>("firebaseResource")

        project.extensions.findByType(AndroidComponentsExtension::class)?.onVariants { variant ->
            val configFile = findConfigFile(
                project.files(
                    getJsonLocations(
                        variant.buildType,
                        variant.productFlavors.map { it.second })
                )
            )

            configFile.inputStream().buffered().use { config ->

                val googleServices =
                    Json.decodeFromStream<GoogleServices>(config)

                val resValues: MutableMap<String, String?> = mutableMapOf()
                handleProjectNumberAndProjectId(googleServices, resValues)
                handleFirebaseUrl(googleServices, resValues)
                val clientObject = getClientForPackageName(googleServices, variant.namespace.get())
                if (clientObject != null) {
                    handleAnalytics(clientObject, resValues)
                    handleMapsService(clientObject, resValues)
                    handleGoogleApiKey(clientObject, resValues)
                    handleGoogleAppId(clientObject, resValues)
                    handleWebClientId(clientObject, resValues)
                } else {
                    throw GradleException("No matching client found for package name '" + variant.namespace.get() + "'")
                }

                resValues.map {
                    variant.makeResValueKey(it.key, "string") to ResValue(it.value.orEmpty())
                }.forEach { (key, value) ->
                    variant.resValues.put(key, value)
                }
            }
        }
    }
}
