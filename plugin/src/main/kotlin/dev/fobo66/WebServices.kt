/*
 * Copyright 2023 Andrey Mukamolov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.fobo66

import dev.fobo66.entities.Client
import org.gradle.api.GradleException

/**
 * Handle a client object for web client
 *
 * @param clientObject the client Json object.
 */
fun handleWebClientId(clientObject: Client, resValues: MutableMap<String, String?>) {
    val oauthClients = clientObject.oauthClient
    oauthClients.find { it.clientType == OAUTH_CLIENT_TYPE_WEB }?.let { client ->
        resValues["default_web_client_id"] = client.clientId
    }
}

/**
 * Handle a client object for analytics (@xml/global_tracker)
 *
 * @param clientObject the client Json object.
 */
fun handleAnalytics(clientObject: Client, resValues: MutableMap<String, String?>) {
    val trackingId = clientObject.services.analyticsService
        ?.analyticsProperty?.trackingId ?: return
    resValues["ga_trackingId"] = trackingId
}

fun handleGoogleApiKey(clientObject: Client, resValues: MutableMap<String, String?>) {
    val apiKey = getAndroidApiKey(clientObject)
    if (apiKey != null) {
        resValues["google_api_key"] = apiKey
        resValues["google_crash_reporting_api_key"] = apiKey
        return
    }
    throw GradleException("Missing api_key/current_key object")
}
