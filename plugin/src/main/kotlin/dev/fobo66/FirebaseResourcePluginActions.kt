/*
 * Copyright 2023 Andrey Mukamolov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.fobo66

import dev.fobo66.entities.Client
import dev.fobo66.entities.GoogleServices
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import java.io.File
import java.util.*

const val OAUTH_CLIENT_TYPE_WEB = 3
const val JSON_FILE_NAME = "google-services.json"

fun handleFirebaseUrl(rootObject: GoogleServices, resValues: MutableMap<String, String?>) {
    val firebaseUrl = rootObject.projectInfo.firebaseUrl
    resValues["firebase_database_url"] = firebaseUrl
}

/**
 * Handle project_info/project_number for @string/gcm_defaultSenderId, and fill the res map with
 * the read value.
 *
 * @param rootObject the root Json object.
 */
fun handleProjectNumberAndProjectId(
    rootObject: GoogleServices,
    resValues: MutableMap<String, String?>
) {
    val projectInfo = rootObject.projectInfo
    val projectNumber = projectInfo.projectNumber
    resValues["gcm_defaultSenderId"] = projectNumber
    val projectId = projectInfo.projectId
    resValues["project_id"] = projectId
    val bucketName = projectInfo.storageBucket
    if (bucketName != null) {
        resValues["google_storage_bucket"] = bucketName
    }
}

/**
 * Handle a client object for maps (@string/google_maps_key).
 *
 * @param clientObject the client Json object.
 */
fun handleMapsService(clientObject: Client, resValues: MutableMap<String, String?>) {
    clientObject.services.mapsService?.let {
        val apiKey = getAndroidApiKey(clientObject)
        if (apiKey != null) {
            resValues["google_maps_key"] = apiKey
            return
        }
        throw GradleException("Missing api_key/current_key object")
    }
}

fun getAndroidApiKey(clientObject: Client): String? {
    val apiKeys = clientObject.apiKey
    return apiKeys.firstOrNull()?.currentKey
}

/**
 * find an item in the "client" array that match the package name of the app
 *
 * @param services the root json object.
 * @return a JsonObject representing the client entry or null if no match is found.
 */
fun getClientForPackageName(services: GoogleServices, applicationId: String): Client? {
    val clients = services.client
    return clients?.find {
        val clientInfo = it.clientInfo
        val androidClientInfo = clientInfo.androidClientInfo
        val clientPackageName = androidClientInfo.packageName
        applicationId == clientPackageName
    }
}

/** Handle a client object for Google App ID.  */
fun handleGoogleAppId(clientObject: Client, resValues: MutableMap<String, String?>) {
    val clientInfo = clientObject.clientInfo
    val googleAppId = clientInfo.mobilesdkAppId
    if (googleAppId.isEmpty()) {
        throw GradleException(
            ("Missing Google App Id. "
                    + "Please follow instructions on https://firebase.google.com/ to get a valid "
                    + "config file that contains a Google App Id")
        )
    }
    resValues["google_app_id"] = googleAppId
}

fun getJsonLocations(buildType: String?, flavorNames: List<String>): Set<String> {
    val flavorName = processFlavorNames(flavorNames)

    val fileLocations = mutableListOf(
        "",
        "src/$flavorName/$buildType",
        "src/$buildType/$flavorName",
        "src/$flavorName",
        "src/$buildType",
        "src/$flavorName${
            buildType?.replaceFirstChar {
                if (it.isLowerCase()) {
                    it.titlecase(Locale.getDefault())
                } else {
                    it.toString()
                }
            }
        }",
        "src/$buildType"
    )
    var fileLocation = "src"
    for (flavor: String in flavorNames) {
        fileLocation += "/$flavor"
        fileLocations.add(fileLocation)
        fileLocations.add("$fileLocation/$buildType")
        fileLocations.add(fileLocation + buildType?.replaceFirstChar {
            if (it.isLowerCase()) {
                it.titlecase(Locale.getDefault())
            } else {
                it.toString()
            }
        })
    }

    return fileLocations
        .asSequence()
        .distinct()
        .sortedByDescending { input: String ->
            countSlashes(
                input
            )
        }
        .map { location: String ->
            if (location.isEmpty()) {
                location + JSON_FILE_NAME
            } else {
                "$location/$JSON_FILE_NAME"
            }
        }
        .toSet()
}

fun processFlavorNames(flavorNames: List<String>): String? {
    val flavorName = flavorNames.stream().reduce(
        ""
    ) { a: String, b: String ->
        a + (if (a.isEmpty()) {
            b
        } else {
            b.replaceFirstChar {
                if (it.isLowerCase()) {
                    it.titlecase(Locale.getDefault())
                } else {
                    it.toString()
                }
            }
        })
    }
    return flavorName
}

fun findConfigFile(configFileLocations: FileCollection): File {
    return configFileLocations.find { it.isFile && it.name == JSON_FILE_NAME }
        ?: throw GradleException(
            "File $JSON_FILE_NAME is missing. The Firebase Resources Plugin cannot function without it. " +
                    "Searched Location: ${configFileLocations.asPath}"
        )
}

private fun countSlashes(input: String): Long {
    return input.codePoints().filter { x: Int -> x == '/'.code }.count()
}
