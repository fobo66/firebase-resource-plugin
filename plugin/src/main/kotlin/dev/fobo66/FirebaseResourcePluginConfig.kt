/*
 * Copyright 2023 Andrey Mukamolov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.fobo66

import org.gradle.api.file.FileCollection
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property

@Suppress("UnnecessaryAbstractClass") // Gradle needs extensions to be abstract classes
abstract class FirebaseResourcePluginConfig {
    abstract val disableVersionCheck: Property<Boolean>

    abstract val defaultConfigFile: RegularFileProperty

    abstract val flavorConfigFiles: Property<FileCollection>

    init {
        disableVersionCheck.convention(false)
    }
}
