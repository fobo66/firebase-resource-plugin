/*
 * Copyright 2023 Andrey Mukamolov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.fobo66

import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertFails
import kotlin.test.assertNotNull

class FirebaseResourcePluginActionsTest {

    @Test fun `no file in collection`() {
        val project = ProjectBuilder.builder().build()
        val fileCollection = project.files("google-services.json")

        assertFails {
            findConfigFile(fileCollection)
        }
    }

    @Test fun `one file in collection`() {
        val project = ProjectBuilder.builder().build()
        project.file("google-services.json").createNewFile()
        val fileCollection = project.files("google-services.json")

        assertNotNull(findConfigFile(fileCollection))
    }
}
